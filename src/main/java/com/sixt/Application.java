package com.sixt;

import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrix;

import java.io.IOException;

public class Application {
    public static void main(String[] args) {
        DistanceMatrix d = null;
        try {
            d = DistanceMatrixBuilder.createDistanceMatrix();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ApiException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(d.rows);

    }
}
