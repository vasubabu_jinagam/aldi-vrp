package com.sixt;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.TravelMode;
import lombok.Data;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

class DistanceMatrixBuilder {

    static DistanceMatrix createDistanceMatrix() throws InterruptedException, ApiException, IOException {
        ArrayList<String> addresses = new ArrayList<>(Arrays.asList(
                "3610+Hacks+Cross+Rd+Memphis+TN",
                "1921+Elvis+Presley+Blvd+Memphis+TN",
                "149+Union+Avenue+Memphis+TN",
                "1034+Audubon+Drive+Memphis+TN",
                "1532+Madison+Ave+Memphis+TN",
                "706+Union+Ave+Memphis+TN",
                "3641+Central+Ave+Memphis+TN",
                "926+E+McLemore+Ave+Memphis+TN",
                "4339+Park+Ave+Memphis+TN",
                "600+Goodwyn+St+Memphis+TN",
                "2000+North+Pkwy+Memphis+TN",
                "262+Danny+Thomas+Pl+Memphis+TN",
                "125+N+Front+St+Memphis+TN",
                "5959+Park+Ave+Memphis+TN",
                "814+Scott+St+Memphis+TN",
                "1005+Tillman+St+Memphis+TN"));

        int maxElements = 100;
        int numberOfAddresses = addresses.size();
        int maxRows = Math.floorDiv(maxElements, numberOfAddresses);
        long[] distanceMatrix = new long[10000];
        ArrayList<String> destAddresses = addresses;
        divModResult d = divMod(numberOfAddresses, maxRows);

        for (int i = 0; i < d.getQuotient(); i++) {
            ArrayList<String> originAddresses = new ArrayList<>(addresses.subList(i * maxRows, (i + 1) * maxRows));
            
        }
        return null;
    }

    private static DistanceMatrix sendRequest(ArrayList<String> originAddresses, ArrayList<String> destAddresses) {
        String apiKey = "AIzaSyAcw6rFQvv55dwJht5Y3aej13hQL02zzTE";
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey(apiKey)
                .build();

        DistanceMatrixApiRequest req = DistanceMatrixApi.newRequest(context);
        String[] origins = originAddresses.stream().toArray(String[]::new);
        String[] destinations = destAddresses.stream().toArray(String[]::new);

        try {
            return req.origins(origins).destinations(destinations).mode(TravelMode.DRIVING).await();
        } catch (ApiException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String buildAddressesString(ArrayList<String> addresses) {
        StringBuilder response = new StringBuilder();
        for (int i = 0; i < addresses.size() - 1; i++) {
            response.append(addresses.get(i)).append('|');
        }
        return response.deleteCharAt(response.length() - 1).toString();
    }




    static divModResult divMod(Integer a, Integer b) {
        divModResult d = new divModResult();
        d.setQuotient(a / b);
        d.setRemainder(a % b);
        return d;
    }
}


@Data
class divModResult {
    Integer remainder;
    Integer quotient;
}




